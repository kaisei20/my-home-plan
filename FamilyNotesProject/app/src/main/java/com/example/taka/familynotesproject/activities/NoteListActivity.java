package com.example.taka.familynotesproject.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonReader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.taka.familynotesproject.R;
import com.example.taka.familynotesproject.models.Note;
import com.example.taka.familynotesproject.utilities.DateConverter;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NoteListActivity extends AppCompatActivity {

    public static final String EXTRA_NOTE_ID = "com.example.taka.familynotesproject.activities.NOTE_ID";
    public static final String TAG = NoteListActivity.class.getSimpleName();
    public static final String BASE_API_URL = "http://10.0.2.2:8880/"; //Emulator's IP Address


    TextView tvDate;
    ListView lvNotes;
    ArrayList<Note> noteList;
    ArrayAdapter<Note>noteAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list);

        init();

    }
    /**
     *Initialize views and components
     */
    private void init(){

        lvNotes = (ListView) findViewById(R.id.lvNotes);
        noteList = new ArrayList<>();
        noteAdapter = new NoteArrayAdapter(this,noteList);
        lvNotes.setAdapter(noteAdapter);

        // Today's Date
        Date now = new Date(System.currentTimeMillis());
        String nowText = DateConverter.convertDateToString(now);
        tvDate = (TextView)findViewById(R.id.tvDate);
        tvDate.setText(nowText);

        editNoteClickListener();
        removeNoteLongClickListener();

        // FloatingActionButton
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NoteListActivity.this,NewNoteActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    protected void onStart(){
        super.onStart();
        new LoadNoteListAsyncTask().execute();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.add_menu, menu);
//        return true;
//    }

    /**
     * Get called when press the menu item"Add" to add a new note, turn to NewNoteActivity
     */

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.miAdd:
//                Intent intent = new Intent(this, NewNoteActivity.class);
//                startActivity(intent);
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    /**
     * Get called when a note from noteList is clicked, turn to NewNoteActivity for Editing
     */
    public void editNoteClickListener() {
        //Anonyous inner class wish event handler
        lvNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //ItemView was clicked, find out which String Note was displaying
                Note selectedNote = (Note) parent.getItemAtPosition(position);

                int noteId = selectedNote.getId();


                Intent intent = new Intent(getApplicationContext(), NoteDetailActivity.class);

                intent.putExtra(EXTRA_NOTE_ID, noteId);
                intent.putExtra("time", selectedNote.getNoteTime());

                intent.putExtra("title",selectedNote.getTitle());
                intent.putExtra("detail",selectedNote.getDetail());
                intent.putExtra("member",selectedNote.getMember());

                intent.putExtra("priorityType",selectedNote.getPriority());
                startActivity(intent);

//                Intent intent = new Intent(getApplicationContext(), NoteDetailActivity.class);
//                intent.putExtra(EXTRA_NOTE_ID, noteId);
//
//                startActivity(intent);

                // For Toast displaying only
                String note = selectedNote.getTitle() + " " + selectedNote.getNoteDate() + " ";
                Toast.makeText(getApplicationContext(), note, Toast.LENGTH_LONG).show();

                // Logging
                Log.d(TAG, "Selected Note " + note);

            }
        });
    }

    /**
     * Get called when a note from noteList is long-clicked, show a dialog to confirm removing it
     */
    public void removeNoteLongClickListener() {
        // Anonymous inner class with event handler
        lvNotes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // ItemView was clicked, find out which String note was displaying
                Note selectedNote = (Note) parent.getItemAtPosition(position);

                int noteId = selectedNote.getId();

                // Show a dialog and allow user to confirm if the selected friend will be removed
                showRemoveNoteDialog(selectedNote);
                Toast.makeText(NoteListActivity.this, "note id: => " + noteId, Toast.LENGTH_LONG).show();
                return true;
            }
        });
    }

    private void showRemoveNoteDialog(final Note selectedNote) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String msgRemoveNote = "Remove this note?";

        builder.setIcon(android.R.drawable.ic_delete);
        builder.setTitle("Remove Note");
        builder.setMessage(msgRemoveNote);

        builder.setPositiveButton("Remove", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteNote(selectedNote);
            }
        });
        builder.setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    /**
     * Inner class for custom array adapter
     */
    public class NoteArrayAdapter extends ArrayAdapter<Note> {
        private Context context;
        private List<Note> list;

        public NoteArrayAdapter(Context context, List<Note> list) {
            super(context, R.layout.note_item, list);
            this.context = context;
            this.list = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View viewNote = convertView;
            if (viewNote == null) {
                viewNote = inflater.inflate(R.layout.note_item, parent, false);
            }

            ImageView ivNotePhotoItem = (ImageView) viewNote.findViewById(R.id.ivNotePhotoItem);
            TextView tvFirstLine = (TextView) viewNote.findViewById(R.id.tvTitlePriorityItem);
            TextView tvSecondLine = (TextView) viewNote.findViewById(R.id.tvDateTimeItem);

            Note note = list.get(position);
            switch (note.getMember()) {
                case All:
                    ivNotePhotoItem.setImageResource(R.drawable.icon_all);
                    break;
                case Father:
                    ivNotePhotoItem.setImageResource(R.drawable.icon_f);
                    break;
                case Mother:
                    ivNotePhotoItem.setImageResource(R.drawable.icon_m);
                    break;
                case Boy:
                    ivNotePhotoItem.setImageResource(R.drawable.icon_b);
                    break;
                case Girl:
                    ivNotePhotoItem.setImageResource(R.drawable.icon_g);
                    break;
                default:
                    break;
            }

//            ivNotePhotoItem.setImageResource(R.drawable.icon_who);

            String title = note.getTitle()+ " : "+note.getDetail();
            tvFirstLine.setText(title);

            Date date = note.getNoteDate();
            Date time = note.getNoteTime();
            String dateTime = date + " | " + time;
//            String date1 = DateConverter.convertDateToString(date);
//            String time1 = DateConverter.convertDateToString(time);
//            String datetime = date1 + " " + time1;
//            tvSecondLine.setText(dateTime);

            tvSecondLine.setText(DateConverter.convertDateToString(date));

            return viewNote;
        }
    }

    /**
     * Inner class for fetching data from MySQL
     */
    class LoadNoteListAsyncTask extends AsyncTask<Void, Void, ArrayList<Note>> {

        @Override
        protected ArrayList<Note> doInBackground(Void... voids) {

            return getDataFromHttpRequest("GET", "notes");

        }

        @Override
        protected void onPostExecute(ArrayList<Note> result) {
            refreshNoteList(result);
        }
    }

    /**
     * Inner class for deleting a note by id   from database
     */
    class DeleteNoteFromListAsyncTask extends AsyncTask<Note, Void, Void> {

        @Override
        protected Void doInBackground(Note... params) {

            getDataFromHttpRequest("DELETE", "notes/" + params[0].getId());
            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            new LoadNoteListAsyncTask().execute(); // Reload the list
        }

    }

    /**
     * Get data result from http request
     *
     * @param restfulUrl
     * @return
     */
    public ArrayList<Note> getDataFromHttpRequest(String httpRequest, String restfulUrl) {

        String TAG = "CRUDNoteListAsyncTask";

        InputStreamReader inputStreamReader;
        JsonReader jsonReader;

        try {
            Log.v(TAG, httpRequest + " notes");

            ArrayList<Note> result = new ArrayList<>();
            URL url = new URL(NoteListActivity.BASE_API_URL + restfulUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(httpRequest);
            inputStreamReader = new InputStreamReader(conn.getInputStream());

            int httpCode = conn.getResponseCode();
            if (httpCode / 100 != 2) { // Not 2xx means issues
                throw new IOException("Invalid HTTP Code response: " + httpCode);
            }

            jsonReader = new JsonReader(inputStreamReader);
            jsonReader.beginArray();

            while (jsonReader.hasNext()) {
                String key;
                jsonReader.beginObject();

                // id from familynotes.notes in MySQL
                //below need modification!!!

                // todo: database total 9 fields(using 8)
                //member from familyNnotes.notes in MySQL
                key = jsonReader.nextName();
                if (!key.equals("id")) throw new ParseException("id expected when parsing", 0);
                int id = jsonReader.nextInt();

                key = jsonReader.nextName();
                if (!key.equals("user_id")) throw new ParseException("user_id expected when parsing", 0);
                int userId = jsonReader.nextInt();

                key = jsonReader.nextName();
                if (!key.equals("member")) throw new ParseException("member expected when parsing", 0);
                Note.MemberType member = Note.MemberType.valueOf(jsonReader.nextString());

                // title from familynotes.notes in MySQL
                key = jsonReader.nextName();
                if (!key.equals("title")) throw new ParseException("title expected when parsing", 0);
                String title = jsonReader.nextString();

                // detail from familynotes.notes in MySQL
                key = jsonReader.nextName();
                if (!key.equals("detail")) throw new ParseException("detail expected when parsing", 0);
                String detail = jsonReader.nextString();

                key = jsonReader.nextName();
                if (!key.equals("note_date")) throw new ParseException("note_date expected when parsing", 0);
                Date noteDate = DateConverter.convertStringToDate(jsonReader.nextString());

                // title from familynotes.notes in MySQL
                key = jsonReader.nextName();
                if (!key.equals("note_time")) throw new ParseException("note_time expected when parsing", 0);
                Date noteTime = DateConverter.convertStringToDate(jsonReader.nextString());

                // picture from familynotes.notes in MySQL
                key = jsonReader.nextName();
                if (!key.equals("picture")) throw new ParseException("picture expected when parsing", 0);
                String picture = jsonReader.nextString();
//                String picture = "";
//                if (jsonReader==null)

                // priorityType from familynotes.notes in MySQL
                key = jsonReader.nextName();
                if (!key.equals("priority")) throw new ParseException("priority expected when parsing", 0);
                Note.PriorityType priority = Note.PriorityType.valueOf(jsonReader.nextString());

                jsonReader.endObject();
                Note note = new Note(userId,member,title,detail,noteDate,noteTime,picture,priority);
                result.add(note);

                Log.v(TAG, "Note parsed and added to list: " + noteList.toString());
            }

            jsonReader.endArray();

            return result;
        } catch (IOException | ParseException ex) {
            Log.e(TAG, "Exception reading from URL", ex);
            return null;
        }
    }

    /**
     * Delete a selected note by id from database
     *
     * @param note
     */
    public void deleteNote(Note note) {
        new DeleteNoteFromListAsyncTask().execute(note);
        noteAdapter.notifyDataSetChanged();
        Toast.makeText(getApplicationContext(), "Note [ " + note.getTitle() + " ] removed", Toast.LENGTH_LONG).show();
        Log.d(TAG, "Note [ " + note.getTitle() + " ] Removed ");
    }

    /**
     * Refresh the results of notelist from database
     *
     * @param list
     */
    public void refreshNoteList(List<Note> list) {
        noteList.clear();

        if (list == null) {
            Toast.makeText(getApplicationContext(), "API error fetching data", Toast.LENGTH_LONG).show();
            return;
        }

        noteList.addAll(list);
        noteAdapter.notifyDataSetChanged();
    }


}
