package com.example.taka.familynotesproject.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.taka.familynotesproject.R;
import com.example.taka.familynotesproject.models.Note;
import com.example.taka.familynotesproject.utilities.DateConverter;

import android.content.Intent;

import android.os.Environment;

import android.support.v4.content.FileProvider;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class NewNoteActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener {

    TextView tvNote, tvDate, tvTime, tvPriority;
    EditText etTitle, etDetail;
    ImageButton ibDate, ibTime, ibWho, ibCamera, ibMap;
    RadioGroup radioGroup;
    Button btSave;
    private TextView tvWho;
    private DialogFragment dialogFragment;
    private FragmentManager fragmentManager;
    //  Camera
    private final static int RESULT_CAMERA = 1001;
    private final static int REQUEST_PERMISSION = 1002;

    private ImageView ivNotePicture;
    private Uri cameraUri;
    private String filePath;
    public Note.PriorityType priorityType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("debug", "onCreate()");
        setContentView(R.layout.activity_new_note);

//        tvNote = (TextView) findViewById(R.id.tvNote);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvTime = (TextView) findViewById(R.id.tvTime);
        tvPriority = (TextView) findViewById(R.id.tvPriority);
        etTitle = (EditText) findViewById(R.id.etTitle);
        etDetail = (EditText) findViewById(R.id.etDetail);
        ibMap = findViewById(R.id.ibMap);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        tvWho = findViewById(R.id.tvWho);
        ibWho = findViewById(R.id.ibWho);
        btSave = findViewById(R.id.btSave);


        priorityType = getPriorityType();

        ibWho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                dialogFragment = new AlertDialogFragment();
                dialogFragment.show(fragmentManager, "test alert dialog");
            }
        });

        ibMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewNoteActivity.this,MapsActivity.class);
                startActivity(intent);
            }
        });

        ivNotePicture = findViewById(R.id.ivNotePicture);

        ImageButton cameraButton = findViewById(R.id.ibCamera);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Android 6, API 23
                if (Build.VERSION.SDK_INT >= 23) {
                    checkPermission();
                } else {
                    cameraIntent();
                }
            }
        });
    }

    public static class AlertDialogFragment extends DialogFragment {
        private String[] menulist = {"All", "Father", "Mother", "Boy", "Girl"};

        @Override
        @NonNull
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

            alert.setTitle("Who");
            alert.setItems(menulist, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int idx) {

                    if (idx == 0) {
                        setMessage(menulist[0]);
                    } else if (idx == 1) {
                        setMessage(menulist[1]);
                    } else if (idx == 2) {
                        setMessage(menulist[2]);
                    } else if (idx == 3) {
                        setMessage(menulist[3]);
                    } else {
                        setMessage(menulist[4]);
                    }
                }
            });
            return alert.create();
        }

        private void setMessage(String message) {
            NewNoteActivity newNoteActivity = (NewNoteActivity) getActivity();
            newNoteActivity.setMember(message);
        }
    }

    public void setMember(String message) {
        tvWho.setText(message);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String str = String.format(Locale.US, "%d-%d-%d", year, monthOfYear + 1, dayOfMonth);
        tvDate.setText(str);
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newDateFragment = new DatePick();
        newDateFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String str = String.format(Locale.US, "%d:%d", hourOfDay, minute);
        tvTime.setText(str);
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newTimeFragment = new TimePick();
        newTimeFragment.show(getSupportFragmentManager(), "timePicker");
    }


    private Note.PriorityType getPriorityType() {
        Note.PriorityType priorityType = null;
        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.rb1:
//                priorityType = 1;
                priorityType = Note.PriorityType.Low;
                break;
            case R.id.rb2:
//                priorityType = 2;
                priorityType = Note.PriorityType.Medium;
                break;
            case R.id.rb3:
//                priorityType = 3;
                priorityType = Note.PriorityType.High;
                break;
//            default:
//                priorityType = 0;
        }
        return priorityType;
    }

    public static Note.MemberType convertStringMemberType(String stringType) {
        Note.MemberType memberType = null;
        switch (stringType) {
            case "All":
                memberType = Note.MemberType.All;
                break;
            case "Father":
                memberType = Note.MemberType.Father;
                break;
            case "Mother":
                memberType = Note.MemberType.Mother;
                break;
            case "Boy":
                memberType = Note.MemberType.Boy;
                break;
            case "Girl":
                memberType = Note.MemberType.Girl;
                break;
        }
        return memberType;
    }

    private void cameraIntent() {
        Log.d("debug", "cameraIntent()");

        // Save to Folder
//        File cameraFolder = new File(
//                Environment.getExternalStoragePublicDirectory(
//                        Environment.DIRECTORY_PICTURES),"IMG");
//        cameraFolder.mkdirs();

        // Save to Camera
        File cameraFolder = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DCIM), "Camera");

        String fileName = new SimpleDateFormat(
                "ddHHmmss", Locale.US).format(new Date());
        filePath = String.format("%s/%s.jpg", cameraFolder.getPath(), fileName);
        Log.d("debug", "filePath:" + filePath);

        // capture file path
        File cameraFile = new File(filePath);
        cameraUri = FileProvider.getUriForFile(
                NewNoteActivity.this,
                getApplicationContext().getPackageName() + ".fileprovider",
                cameraFile);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
        startActivityForResult(intent, RESULT_CAMERA);

        Log.d("debug", "startActivityForResult()");
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent intent) {
        if (requestCode == RESULT_CAMERA) {

            if (cameraUri != null) {
                ivNotePicture.setImageURI(cameraUri);

                registerDatabase(filePath);
            } else {
                Log.d("debug", "cameraUri == null");
            }
        }
    }

    // Database of Android
    private void registerDatabase(String file) {
        ContentValues contentValues = new ContentValues();
        ContentResolver contentResolver = NewNoteActivity.this.getContentResolver();
        contentValues.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        contentValues.put("_data", file);
        contentResolver.insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
    }

    // Runtime Permission check
    private void checkPermission() {

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            cameraIntent();
        }

        else {
            requestPermission();
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(NewNoteActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION);

        } else {
            Toast toast = Toast.makeText(this,
                    "permission require",
                    Toast.LENGTH_SHORT);
            toast.show();

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,},
                    REQUEST_PERMISSION);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        Log.d("debug", "onRequestPermissionsResult()");

        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                cameraIntent();

            } else {
                Toast toast = Toast.makeText(this,
                        "can't do anything", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }


    public void onClickSave(View view) {
        addNote();
        Intent intent = new Intent(this, NoteListActivity.class);
        startActivity(intent);
        finish();
    }


    /**
     * Get note from NoteListActivity and set all properties to current components for editing
     */
    public void setSelectedNote(Note selectedNote) {

        etTitle.setText(selectedNote.getTitle());
        etDetail.setText(selectedNote.getDetail());

    }

    /**
     * Add a new note to remote database
     */
    private void addNote() {

        int userId = LoginActivity.currentUser.getId();
        Note.MemberType member = convertStringMemberType(tvWho.getText().toString());
        String title = etTitle.getText().toString();
        String detail = etDetail.getText().toString();
        Date noteDate = DateConverter.convertStringToDate(tvDate.getText().toString());
        Date noteTime = DateConverter.convertStringToDate(tvTime.getText().toString());
        String picture = filePath;
        Note.PriorityType priority = priorityType;

        Note note = new Note(
                userId,
                member,
                title,
                detail,
                noteDate,
                noteTime,
                picture,
                priority
        );

        new InsertNoteAsyncTask().execute(note);

    }

    class InsertNoteAsyncTask extends AsyncTask<Note, Void, Boolean> {

        public static final String TAG = "InsertNoteAsyncTask";

        @Override
        protected Boolean doInBackground(Note... params) {

            Note note = params[0];
            HttpURLConnection conn = null;
            PrintWriter printWriter = null;

            try {
                Log.v(TAG, "POST / notes");

                URL url = new URL(NoteListActivity.BASE_API_URL + "/notes");
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setDoInput(false); // Not receiving data
                conn.setDoOutput(true); // But sending data

                //
                printWriter = new PrintWriter(conn.getOutputStream());

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user_id", note.getUserId());
                jsonObject.put("member", note.getMember().toString());
                jsonObject.put("title", note.getTitle());
                jsonObject.put("detail", note.getDetail());
                jsonObject.put("note_date", DateConverter.convertDateToString(note.getNoteDate()));
                jsonObject.put("note_time", DateConverter.convertTimeToString(note.getNoteTime()));
                jsonObject.put("picture", note.getPicture());
                jsonObject.put("priority", note.getPriority().toString());

                String stringJson = jsonObject.toString();

                printWriter.print(stringJson);
                printWriter.flush();
                printWriter.close();

                int httpCode = conn.getResponseCode();
                if (httpCode / 100 != 2)
                    throw new IOException("Invalid HTTP Code response: " + httpCode);
                return true;
            } catch (IOException | JSONException ex) {
                Log.e(TAG, "Exception writing to URL", ex);
                return false;
            } finally { // FIXME: finally missing closing print writer. connection
                if (printWriter != null) {
                    printWriter.close();
                    conn.disconnect();
                }
            }

        }

        @Override
        protected void onPostExecute(Boolean result){
            if(result){
                Toast.makeText(getApplicationContext(),"Note added successful",Toast.LENGTH_LONG).show();
                finish();
            }else{
                Toast.makeText(getApplicationContext(),"API error adding friend",Toast.LENGTH_LONG).show();
            }
        }

    }
}
