package com.example.mobileapps.myfamilynotes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText etEmail;
    EditText etPassword;
    Button btSign_in;
    Button btSign_up;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startRetrofit();

        etEmail=(EditText) findViewById(R.id.etEmail);
        etPassword=(EditText) findViewById(R.id.etPassword);
        btSign_in=(Button) findViewById(R.id.btSign_in);
        btSign_up=(Button) findViewById(R.id.btSign_up);
        btSign_in.setOnClickListener(){

        }
    }

    public void startRetrofit(){

    }
}
