-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 19, 2018 at 06:57 AM
-- Server version: 5.7.24
-- PHP Version: 7.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `familynotes`
--

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('Father','Mother','Child','Grandfather','Grandmother') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `member` enum('All','Father','Mother','Boy','Girl') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `detail` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note_date` date NOT NULL DEFAULT '1970-12-31',
  `note_time` time NOT NULL DEFAULT '00:00:00',
  `picture` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT '""',
  `priority` enum('Low','Medium','High') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Low'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `user_id`, `member`, `title`, `detail`, `note_date`, `note_time`, `picture`, `priority`) VALUES
(1, 1, 'Father', 'Tom Note 1', 'Tom Note 1, Tom Note 1', '2018-12-04', '11:10:00', '\"\"', 'Low'),
(2, 1, 'Mother', 'Tom Note 1', 'Tom Note 2, Tom Note 2', '2018-12-05', '08:30:00', '\"\"', 'High'),
(3, 2, 'Father', 'John Note 1', 'John Note 1, John Note 1', '2018-11-04', '11:10:00', '\"\"', 'High'),
(4, 2, 'Boy', 'John Note 2', 'John Note 2, John Note 2', '2018-01-07', '11:00:00', '\"\"', 'Medium'),
(5, 2, 'Boy', 'John Note 3', 'John Note 3, John Note 3', '2018-01-07', '11:00:00', '\"\"', 'Medium'),
(6, 2, 'Girl', 'John Note 4', 'John Note 4, John Note 4', '2018-02-09', '11:10:00', '\"\"', 'Low'),
(7, 7, 'Mother', 'Kerry Note 1', 'Kerry Note 1, Kerry Note 1', '2018-05-09', '09:00:00', '\"\"', 'Medium'),
(8, 7, 'All', 'Kerry Note 2', 'Kerry Note 2, Kerry Note 2', '2018-06-30', '11:10:00', '\"\"', 'High'),
(9, 7, 'Girl', 'Kerry Note 3', 'Kerry Note 3, Kerry Note 3', '2018-09-01', '07:30:00', '\"\"', 'High'),
(11, 2, 'Mother', 'John Note 5', 'John Note 5, John Note 5', '2018-12-04', '12:10:00', '\"\"', 'High'),
(12, 2, 'Girl', 'John Note 6', 'John Note 6, John Note 6', '2018-12-04', '12:10:00', '\"\"', 'Low'),
(13, 2, 'Father', 'John Note ', 'John Note 7, John Note 7', '2018-12-29', '13:10:00', '\"\"', 'High');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`) VALUES
(1, 'tom@familynotes.com', '111111'),
(2, 'john@familynotes.com', '222222'),
(3, 'mike@familynotes.com', '333333'),
(4, 'mary@familynotes.com', '444444'),
(5, 'rose@familynotes.com', '555555'),
(6, 'jerry@familynotes.com', '666666'),
(7, 'kerry@familynotes.com', '777777'),
(8, 'taka@familynotes.com', '111111'),
(9, 'tom12@familynotes.com', '111111'),
(10, 'tom123@familynotes.com', '111111'),
(11, 'tom12345@familynotes.com', '111111');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_notes` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `fk_users_notes` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
