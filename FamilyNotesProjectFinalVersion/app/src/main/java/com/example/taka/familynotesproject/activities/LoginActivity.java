package com.example.taka.familynotesproject.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.taka.familynotesproject.R;
import com.example.taka.familynotesproject.models.User;
import com.example.taka.familynotesproject.services.UserService;
import com.example.taka.familynotesproject.utilities.Constant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {


    public static final String TAG = LoginActivity.class.getSimpleName();
    public static final String PREF_EMAIL = "email";
    public static final String PREF_PASSWORD = "password";

    UserService userService;

    EditText etEmail;
    EditText etPassword;
    Button btSignin;
    Button btSigninFacebook;
    Button btSigninGoogle;
    Button btSignup;
    SharedPreferences sharedPreferences;
    public static User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

        initService();
    }

    private void init() {

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btSignin = (Button) findViewById(R.id.btSignin);
        btSignup = (Button) findViewById(R.id.btSignup);


    }

    @Override
    protected void onStart() {
        super.onStart();
        //tvUserId.setText(sharedPreferences.getInt(Constant.EXTRA_USER_ID, 0));
        etEmail.setText(sharedPreferences.getString(PREF_EMAIL, ""));
        etPassword.setText(sharedPreferences.getString(PREF_PASSWORD, ""));
    }


    public void onClick(View v) {
        int i = v.getId();
        switch (i) {
            case R.id.btSignin:
                signIn();
                break;
            case R.id.btSignup:
                signup();
                break;
            case R.id.btSigninGoogle:
                break;
            case R.id.btSigninFacebook:
                break;
            default:
                break;
        }
    }
    //signIn call index.api by Retrofit
    // tom@familynotes.com, 111111 ; john@familynotes.com, 222222 for testing
    private void signIn() {

        String email = etEmail.getText().toString();
        final String pwd = etPassword.getText().toString();
//call model class user
        Call<List<User>> loginUser = userService.getUserByEmail(email);

        loginUser.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                List<User> list = response.body();

                if (response.isSuccessful()) {
                    if (list.size() == 0) {
                        Toast.makeText(LoginActivity.this, " Email is wrong!", Toast.LENGTH_LONG).show();
                        return;
                    }
                    User user = list.get(0);
                    if (user.getPassword().equals(pwd)) {
                        currentUser = user;
                        Intent intent = new Intent(LoginActivity.this, NoteListActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(LoginActivity.this, "password is wrong!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "API error 2", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }


    private void signup() {
        final User user = new User();
        user.setEmail(etEmail.getText().toString());
        user.setPassword(etPassword.getText().toString());
//        todo: deal with username;
//        user.setUserName(user.getEmail());

        final Call<Integer> register = userService.register(user);
        register.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {

                if (response.isSuccessful()) {
                    int id = response.body();
                    if (id == -1) {
                        Toast.makeText(LoginActivity.this, "Email is already used.", Toast.LENGTH_LONG).show();
                        return;
                    }
                    user.setId(id);
                    currentUser = user;
                    Toast.makeText(LoginActivity.this, "User added with id " + id, Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(LoginActivity.this, NewNoteActivity.class);
                    startActivity(intent);
                } else {
                    String error = response.errorBody().toString();
                    Toast.makeText(LoginActivity.this, "Register fail :\n" + error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.d(TAG, "onFailure: register action: " + t.getMessage(), t);
                Toast.makeText(LoginActivity.this, t.getMessage() +
                        "check server side", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initService() {
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        userService = retrofit.create(UserService.class);
    }


}








