package com.example.taka.familynotesproject.models;

import java.sql.Time;
import java.util.Date;

public class Note {
    private int id;
    private int userId;
    private MemberType member;
    private String title;
    private String detail;
    private Date noteDate;
    private Date noteTime;
    private String picture;
    private PriorityType priority;

    public Note(int userId, MemberType member, String title, String detail, Date noteDate, Date noteTime, String picture, PriorityType priority) {
        this.userId = userId;
        this.member = member;
        this.title = title;
        this.detail = detail;
        this.noteDate = noteDate;
        this.noteTime = noteTime;
        this.picture = picture;
        this.priority = priority;
    }

    public Note(){

    }

    public enum MemberType {
        All, Father, Mother, Boy, Girl
    }

    public enum PriorityType {
        Low, Medium, High
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public MemberType getMember() {
        return member;
    }

    public void setMember(MemberType member) {
        this.member = member;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Date getNoteDate() {
        return noteDate;
    }

    public void setNoteDate(Date noteDate) {
        this.noteDate = noteDate;
    }

    public Date getNoteTime() {
        return noteTime;
    }

    public void setNoteTime(Date noteTime) {
        this.noteTime = noteTime;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public PriorityType getPriority() {
        return priority;
    }

    public void setPriority(PriorityType priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Note{" +
                ", userId=" + userId +
                ", member=" + member +
                ", title='" + title + '\'' +
                ", detail='" + detail + '\'' +
                ", noteDate=" + noteDate +
                ", noteTime=" + noteTime +
                ", priorityType=" + priority +
                '}';
    }
}
