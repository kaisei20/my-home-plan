package com.example.taka.familynotesproject.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.taka.familynotesproject.R;

import org.w3c.dom.Text;

public class NoteDetailActivity extends AppCompatActivity {

    ImageView ivDetail;
    TextView tvName, tvDetail, tvPriority;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);

        tvName = (TextView) findViewById(R.id.tvName);
        tvDetail = (TextView) findViewById(R.id.tvDetail);
        tvPriority = (TextView) findViewById(R.id.tvPriority);
        ivDetail = (ImageView) findViewById(R.id.ivDetail);


        Intent intent = getIntent();
        String member = intent.getStringExtra("title");
        String detail = intent.getStringExtra("detail");
        String detailPicture = intent.getStringExtra("picture");
        String priority = intent.getStringExtra("priority");



        tvName.setText(member);
        tvDetail.setText(detail);
        //ivDetail.setImage(detailPicture);
        tvPriority.setText(priority);

//  FIXME: Time & Priority
//        Date time = note.getNoteTime();
//        String dateTime = date + " | " + time;
//        tvPriority.setText(DateConverter.convertDateToString(priorityType));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miEdit:
                Intent intent = new Intent(this, NewNoteActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
