package com.example.taka.familynotesproject.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.taka.familynotesproject.R;

public class PickImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_image);
    }
}
