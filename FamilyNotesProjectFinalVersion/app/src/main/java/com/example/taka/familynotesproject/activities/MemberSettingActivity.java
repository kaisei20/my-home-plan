package com.example.taka.familynotesproject.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taka.familynotesproject.R;
import com.example.taka.familynotesproject.utilities.RequestPermissionHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

//import de.hdodenhof.circleimageview.CircleImageView;

public class MemberSettingActivity extends AppCompatActivity {

    public static final String TAG = MemberSettingActivity.class.getSimpleName();
    private static final String IMAGE_DIR = "/Galleries";
    private int REQCODE_GALLERY = 1, REQCODE_CAMERA = 2;
    private RequestPermissionHandler permissionHandler;

    CircleImageView civPhoto;
    ImageView ivEditPhoto;
    TextView tvName;

    EditText etName;

    int memberId;

    String photoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_setting);
        // Initialize the views
        init();
    }

    /**
     * Initialize
     */
    private void init() {

        civPhoto = (CircleImageView) findViewById(R.id.civPhoto);
        tvName = (TextView) findViewById(R.id.tvName);
        etName = (EditText) findViewById(R.id.etName);

        Intent intent = this.getIntent();

//        memberId = intent.getIntExtra(LoginActivity.EXTRA_MEMBER_ID, -1);

//        if (memberId != -1) {
//            new FetchFriendByIdAsyncTask().execute(memberId);
//        }

        permissionHandler = new RequestPermissionHandler();

        ivEditPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });
    }

    /**
     * Inflate the menu based on different click event from LoginActivity
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(memberId == -1 ? R.menu.add_menu : R.menu.edit_menu, menu);
        return true;
    }

    /**
     * Get called when add icon button is clicked
     *
     * @param item
     * @return
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.miAdd:
//                addFriend();
//                return true;
//            case R.id.miSave:
//                updateFriend();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Show a dialog with select options
     */
    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        choosePhotoFromGallery();
                        break;
                    case 1:
                        takePhotoFromCamera();
                        break;
                }
            }
        });
        pictureDialog.show();
    }

    /**
     * Select image from gallery
     */
    private void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, REQCODE_GALLERY);
    }

    /**
     * Capture image from camera
     */
    private void takePhotoFromCamera() {

        permissionHandler.requestPermission(this,
                new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE },
                123, new RequestPermissionHandler.RequestPermissionListener() {

                    @Override
                    public void onSuccess() {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, REQCODE_CAMERA);
                        Toast.makeText(MemberSettingActivity.this, "Request permission success", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailed() {
                        Toast.makeText(MemberSettingActivity.this, "Request permission failed", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }

        if (requestCode == REQCODE_GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Log.d(TAG, "ContentURI: " + contentURI);

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    photoPath = path;
                    civPhoto.setImageBitmap(bitmap);

                    Toast.makeText(MemberSettingActivity.this, "Image Saved!", Toast.LENGTH_LONG).show();
                } catch (IOException ex) {
                    ex.printStackTrace();
                    Toast.makeText(MemberSettingActivity.this, "Failed picking image!", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == REQCODE_CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            civPhoto.setImageBitmap(thumbnail);
            saveImage(thumbnail);
            Toast.makeText(MemberSettingActivity.this, "Image Saved!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Save image to default media directory with timestamp
     *
     * @param bitmap
     * @return
     */
    private String saveImage(Bitmap bitmap) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File friendPhotoDirectory = new File(Environment.getExternalStorageDirectory() + IMAGE_DIR);

        // Have the object build the directory structure, if needed
        if (!friendPhotoDirectory.exists()) {
            friendPhotoDirectory.mkdirs();
        }

        try {
            File file = new File(friendPhotoDirectory, Calendar.getInstance().getTimeInMillis() + ".jpg");
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{file.getPath()},
                    new String[]{"image/jpeg"}, null);
            fileOutputStream.close();
            Log.d(TAG, "File Saved: ==>" + file.getAbsolutePath());

            return file.getAbsolutePath();
        } catch (IOException ex) {
            ex.printStackTrace();
            Log.e(TAG, "Failed to create file", ex);
        }
        return " ";
    }

//    /**
//     * Inner class for inserting a friend into database
//     */
//    class FetchFriendByIdAsyncTask extends AsyncTask<Integer, Void, Friend> {
//
//        @Override
//        protected Friend doInBackground(Integer... params) {
//            return LoginActivity.appDatabase.daoAccess().fetchFriendById(params[0]);
//        }
//
//        @Override
//        protected void onPostExecute(Friend friend) {
//            setSelectedFriend(friend);
//        }
//
//    }
//
//    /**
//     * Inner class for inserting a friend into database
//     */
//    class InsertFriendIntoDbAsyncTask extends AsyncTask<Friend, Void, Boolean> {
//
//        @Override
//        protected Boolean doInBackground(Friend... params) {
//            try {
//                LoginActivity.appDatabase.daoAccess().insertFriend(params[0]);
//                return true;
//            } catch (Exception ex) {
//                Log.e(TAG, "DaoAccess.insertFriend() failed", ex);
//                return false;
//            }
//        }
//
//    }
//
//    /**
//     * Inner class for updating a friend by id into database
//     */
//    class UpdateFriendIntoDbAsyncTask extends AsyncTask<Friend, Void, Void> {
//
//        @Override
//        protected Void doInBackground(Friend... params) {
//            LoginActivity.appDatabase.daoAccess().updateFriend(params[0]);
//            return null;
//        }
//
//    }
//
//    /**
//     * Add a new friend
//     *
//     * @param
//     */
//    public void addFriend() {
//
//        String name = etName.getText().toString();
//
//        String stringPhotoUri = (photoPath != null) ? photoPath : getURLForResource(R.drawable.man);
//
//        new InsertFriendIntoDbAsyncTask().execute(new Friend(0, name, stringPhotoUri));
//
//        finish();
//
//        Toast.makeText(getApplicationContext(), "Friend added", Toast.LENGTH_LONG).show();
//    }
//
//    /**
//     * Get friend from LoginActivity and set all properties to current components for editing
//     */
//    public void setSelectedMember(Member selectedFriend) {
//
//        if (selectedFriend.getPhotoUri() != null) {
//            Uri photoUri = Uri.fromFile(new File(selectedFriend.getPhotoUri()));
//            civPhoto.setImageURI(photoUri);
//        } else {
//            civPhoto.setImageResource(R.drawable.man);
//        }
//
//        String name = selectedMember.getFirstName();
//        tvName.setText(name);
//
//        etName.setText(selectedFriend.getName());
//        photoPath = selectedFriend.getPhotoUri();
//
//    }
//
//    /**
//     * Update selected friend
//     */
//    private void updateFriend() {
//
//        Friend editedFriend = new Friend();
//
//        editedFriend.setId(friendId);
//        editedFriend.setFirstName(etFirstName.getText().toString());
//        editedFriend.setLastName(etLastName.getText().toString());
//        editedFriend.setDateOfBirth(DateConverter.fromTimestamp(etDateOfBirth.getText().toString()));
//        editedFriend.setPhotoUri(photoPath);
//
//        new UpdateFriendIntoDbAsyncTask().execute(editedFriend);
//
//        finish(); // Dismissed the activity, pops it from the stack
//
//        Toast.makeText(getApplicationContext(), "Friend saved", Toast.LENGTH_SHORT).show();
//    }
//
//    /**
//     *
//     * @param resourceId
//     * @return
//     */
//    public String getURLForResource (int resourceId) {
//        return Uri.parse("android.resource://" + R.class.getPackage().getName() + "/" + resourceId).toString();
//    }

}
