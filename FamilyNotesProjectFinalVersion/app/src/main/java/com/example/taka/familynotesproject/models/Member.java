package com.example.taka.familynotesproject.models;

public class Member {
    private int id;
    //private user_id;
    private String name;
    private int member_icon;

    public Member(int id, String name, int member_icon) {
        this.id = id;
        this.name = name;
        this.member_icon = member_icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMember_iconNumber() {
        return member_icon;
    }

    public void setMember_icon(int member_icon) {
        this.member_icon = member_icon;
    }
}
