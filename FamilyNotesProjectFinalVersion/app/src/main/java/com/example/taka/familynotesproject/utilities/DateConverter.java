package com.example.taka.familynotesproject.utilities;

//import android.arch.persistence.room.TypeConverter;
//import android.arch.persistence.room.TypeConverter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateConverter {
    private static final String TIME_STAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String TIME_FORMAT = "HH:mm";

    private static DateFormat datetimeFormatter = new SimpleDateFormat(TIME_STAMP_FORMAT, Locale.CANADA);
    private static DateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT, Locale.CANADA);
    private static DateFormat timeFormatter = new SimpleDateFormat(TIME_FORMAT, Locale.CANADA);

    /**
     * Convert string formatted date to Date type
     *
     * @param value
     * @return
     */
//    @TypeConverter
    public static Date convertStringToDate(String value) {
        if (value != null) {
            try {
                return dateFormatter.parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return null;
        }
    }

    /**
     * Convert Date type to string formatted value
     *
     * @param value
     * @return
     */
//    @TypeConverter
    public static String convertDateToString(Date value) {
        return value == null ? null : dateFormatter.format(value);
    }

    /**
     * Convert long type milliseconds to string formatted time
     *
     * @param mills
     * @return
     */
//    @TypeConverter
    public static String convertMillisToString(long mills) {
        Date date = new Date(mills);
        return mills == 0 ? null : timeFormatter.format(date);
    }

    /**
     * Convert date type to string formatted time
     *
     * @param value
     * @return
     */
//    @TypeConverter
    public static String convertTimeToString(Date value) {
        return value == null ? null : timeFormatter.format(value);
    }

        /**
         * Convert string formatted time to milliseconds
         *
         * @param value
         * @return
         */
//    @TypeConverter
    public static long convertStringToMills(String value) {
        Date date = convertStringToDate(value);
        return date.getTime();
    }

}
