<?php
/**
 * Created by PhpStorm.
 * User: mobileapps
 * Date: 2018-12-04
 * Time: 10:04
 */

/**
 * Return false if authentication is missing or invalid
 * Return users table record of authenticated user if user/pass was correct
 *
 * @return bool|mixed
 */
function getAuthUser() {
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        return false;
    }
    $email = $_SERVER['PHP_AUTH_USER'];
    $pass = $_SERVER['PHP_AUTH_PW'];
    $auth_user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);

    if (!$auth_user) {
        return false;
    }

    if ($auth_user['password'] == $pass) {
        unset($auth_user['password']);
        return $auth_user;
    } else {
        return false;
    }
}

/**
 * Return true if data is valid, otherwise string describing the problem
 *
 * @param $user
 * @return bool|string
 */
function isUserValid($user) {
    if (is_null($user)) return "JSON parsing failed, user is null";
    if (count($user) != 3) return "Invalid number of values received";
    if (isset($user['id'])) unset($user['id']);
    if (filter_var($user['email'], FILTER_VALIDATE_EMAIL) === false) return "email is invalid";
    // TODO: require quality passwords, e.g. one upper-case, one lower-case, one digit or special character
    if (strlen($user['password']) < 8) return "Password too short, must be 8 characters minimum";
    return true;
}

/**
 * @param $note
 * @return bool|string
 */
function isNoteValid($note) {
    if (is_null($note)) return "JSON parsing failed, note is null";
//    if (count($note) != 8) return "Invalid number of values received";
    if (isset($note['id'])) unset($note['id']);
    if (strlen($note['title']) < 1 || strlen($note['title']) > 100) return "Note too short or too long, must be 1-100 characters";
    return true;
}