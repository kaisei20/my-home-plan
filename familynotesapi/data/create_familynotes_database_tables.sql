-- ----------------------------
-- Create database familynotes
-- ----------------------------
CREATE DATABASE IF NOT EXISTS familynotes;

/* Switch to the current database context */
USE familynotes;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


-- ----------------------------
-- Create table users
-- ----------------------------
DROP TABLE IF EXISTS users;
CREATE TABLE users (
    id                  INT(11)             NOT NULL AUTO_INCREMENT,
    email               VARCHAR(180)        NOT NULL UNIQUE,
    password            VARCHAR(100)        NOT NULL DEFAULT '',
    CONSTRAINT pk_user_id PRIMARY KEY (id ASC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- ----------------------------
-- Insert records for users
-- ----------------------------
INSERT INTO `users`
    (`email`, `password`)
VALUES
    ('tom@familynotes.com', '111111'),
    ('john@familynotes.com', '222222'),
    ('mike@familynotes.com', '333333'),
    ('mary@familynotes.com', '444444'),
    ('rose@familynotes.com', '555555'),
    ('jerry@familynotes.com', '666666'),
    ('kerry@familynotes.com', '777777');


-- ----------------------------
-- Create table members
-- ----------------------------
DROP TABLE IF EXISTS members;
CREATE TABLE members (
       id               INT(11)             NOT NULL AUTO_INCREMENT,
       user_id          INT(11)             NOT NULL,
       name             VARCHAR(100)        NOT NULL DEFAULT '',
       role             ENUM('Father','Mother','Child','Grandfather','Grandmother'),
       photo            VARCHAR(250)        NULL,
       CONSTRAINT pk_member_id PRIMARY KEY (id ASC),
       CONSTRAINT fk_users_members FOREIGN KEY (user_id) REFERENCES users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- ----------------------------
-- Create table notes
-- ----------------------------
DROP TABLE IF EXISTS notes;
CREATE TABLE notes (
    id                  INT(11)             NOT NULL AUTO_INCREMENT,
    user_id             INT(11)             NOT NULL,
    member              ENUM('All','Father','Mother','Boy','Girl'),
    title               VARCHAR(100)        NOT NULL DEFAULT '',
    detail              VARCHAR(200)        NOT NULL DEFAULT '',
    note_date           DATE                NOT NULL DEFAULT '1970-12-31',
    note_time           TIME                NOT NULL DEFAULT '00:00:00',
    picture             VARCHAR(250)        NULL,
    priority            ENUM('Low','Medium','High'),
    CONSTRAINT pk_note_id PRIMARY KEY (id ASC),
    CONSTRAINT fk_users_notes FOREIGN KEY (user_id) REFERENCES users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- ----------------------------
-- Insert records for notes
-- ----------------------------
INSERT INTO `notes`
    (`user_id`, `member`, `title`, `detail`, `note_date`, `note_time`, `priority`)
VALUES
    (1, 'Father', 'Tom Note 1', 'Tom Note 1, Tom Note 1', '2018-12-04', '11:10:00', 'Low'),
    (1, 'Mother', 'Tom Note 1', 'Tom Note 2, Tom Note 2', '2018-12-05', '08:30:00', 'High'),
    (2, 'Father', 'John Note 1', 'John Note 1, John Note 1', '2018-11-04', '11:10:00', 'High'),
    (2, 'Boy', 'John Note 2', 'John Note 2, John Note 2', '2018-01-07', '11:00:00', 'Medium'),
    (2, 'Boy', 'John Note 3', 'John Note 3, John Note 3', '2018-01-07', '11:00:00', 'Medium'),
    (2, 'Girl', 'John Note 4', 'John Note 4, John Note 4', '2018-02-09', '11:10:00', 'Low'),
    (7, 'Mother', 'Kerry Note 1', 'Kerry Note 1, Kerry Note 1', '2018-05-09', '09:00:00', 'Medium'),
    (7, 'All', 'Kerry Note 2', 'Kerry Note 2, Kerry Note 2', '2018-06-30', '11:10:00', 'High'),
    (7, 'Girl', 'Kerry Note 3', 'Kerry Note 3, Kerry Note 3', '2018-09-01', '07:30:00', 'High');


SET FOREIGN_KEY_CHECKS = 1;