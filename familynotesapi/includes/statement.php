<?php
/**
 * Created by PhpStorm.
 * User: mobileapps
 * Date: 2018-12-14
 * Time: 16:44
 */

/**
 * users table
 */

define('QRY_FETCH_USER_BY_EMAIL', 'SELECT * FROM users WHERE email=%s ');
define('QRY_COUNT_USER_BY_EMAIL', 'SELECT COUNT(*) FROM users WHERE email=%s ');
define('QRY_COUNT_USER_BY_ID_EMAIL', 'SELECT COUNT(*) FROM users WHERE id=%i AND email=%s ');


/**
 * members table
 */



/**
 * notes table
 */
//define('QRY_FETCH_ALL_NOTES_BY_USER_ID', 'SELECT notes.member,notes.title,notes.detail,notes.note_date,notes.note_time,notes.picture,notes.priority FROM notes WHERE notes.user_id=%i ');
define('QRY_FETCH_ALL_NOTES_BY_USER_ID', 'SELECT * FROM notes WHERE notes.user_id=2 ');
define('QRY_SORT_ALL_NOTES_BY_TITLE_ASC', 'SELECT * FROM notes WHERE notes.user_id=%i ORDER BY notes.title ASC ');
define('QRY_SORT_ALL_NOTES_BY_TITLE_DESC', 'SELECT * FROM notes WHERE notes.user_id=%i ORDER BY notes.title DESC ');
define('QRY_FETCH_NOTE_BY_ID', 'SELECT * FROM notes WHERE  notes.user_id=%i ');
//define('QRY_COUNT_NOTE_BY_ID', 'SELECT COUNT(*) FROM notes WHERE notes.id=%i AND notes.user_id=%i ');
define('QRY_COUNT_NOTE_BY_ID', 'SELECT COUNT(*) FROM notes WHERE notes.id=%i AND notes.user_id=2 ');

