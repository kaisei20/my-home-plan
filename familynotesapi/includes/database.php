<?php
/**
 * Created by PhpStorm.
 * User: mobileapps
 * Date: 2018-12-04
 * Time: 09:47
 */

define('DB_HOST', 'localhost');
define('DB_PORT', '3306');
define('DB_NAME', 'familynotes');
define('DB_USER', 'familynotes');
define('DB_PASS', 'familynotes');
define('DB_CODE', 'utf8mb4');