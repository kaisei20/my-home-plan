<?php
/**
 * Created by PhpStorm.
 * User: Takashi & Lujia
 * Date: 2018-12-02
 * Time: 22:57
 */

require_once '../vendor/autoload.php';
require_once '../includes/database.php';
require_once '../includes/statement.php';
require_once '../includes/message.php';
require_once '../functions/validation.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// Create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('../logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('../logs/errors.log', Logger::ERROR));

DB::$user = DB_USER;
DB::$password = DB_PASS;
DB::$dbName = DB_NAME;
DB::$encoding = DB_CODE;

DB::$error_handler = 'note_error_handler';
DB::$nonsql_error_handler = 'note_error_handler';

/**
 * @param $params
 */
function note_error_handler($params) {
    global $log;

    if (isset($params['query'])) {
        $log->error(MSG_ERROR_SQL . ": " . $params['error']);
        $log->error(MSG_ERROR_QUERY . ": " . $params['query']);
    } else {
        $log->error(MSG_ERROR_DB . ": " . $params['error']);
    }

    http_response_code(500);
    header('content-type: application/json');
    echo json_encode(HTTP_CODE_500);
    die; // don't want to keep going if a query broke
}

$app = new \Slim\Slim();

$app->response()->header("content-type", 'application/json');

/**
 * Modify PHP Slim main error handler to produce jSON and write to log
 */
$app->error(function (\Exception $e) use ($app, $log) {
    $log->error($e);
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode(HTTP_CODE_500);
});

/**
 *
 */
$app->notFound(function () use ($app) {
    $app->response()->status(404); // It could be commented.
    echo json_encode(HTTP_CODE_404);
});


/*
 * =====================================================================================================================
 * functions for 'users' table
 * =====================================================================================================================
 */


/**
 * Fetch a user record by email from MySQL for Signin
 */
$app->get('/users/:email', function ($email) use($app) {
    
//    $authUser = getAuthUser();
//    if ($authUser === false) {
//        $app->response()->status(401);
//        echo json_encode(HTTP_CODE_401 . ": " . MSG_ERROR_AUTH);
//        return;
//    }
//
//    $results = DB::queryFirstRow(QRY_FETCH_USER_BY_EMAIL, $email);
//
//    if ($results['id'] == $authUser['id']) {
//        unset($results['password']); // Do NOT send password back
//        echo json_encode($results, JSON_PRETTY_PRINT);
//    } else {
//        $app->response()->status(404);
//        echo json_encode(HTTP_CODE_404);
//    }
    
   $user = DB::query("select * from users where email=%s", $email); 
   if($user){
       echo json_encode($user, JSON_PRETTY_PRINT);
       
   }else{
       echo json_encode(HTTP_CODE_404);
   }
});

$app->get('/users', function() {
       $user = DB::query("select * from users"); 
      echo json_encode($user, JSON_PRETTY_PRINT);
 
});



$app->put('/users/:email', function($email) use ($log, $app) {
    // FIXME: verify todo data is valid
    // FIXME: fail with 400 if record does not exist
    // FIXME: fail if email is already used with another account
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify todo data is valid   
    $result = isUserValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }
    // FIXME: fail with 400 if record does not exist
    $hasOldUser = DB::queryFirstField("SELECT COUNT(*) FROM users WHERE id=%i AND email=%s", $authUser['id'], $email);
    if (!$hasOldUser) {
        echo json_encode("404 - unable to update non-existing record");
        $app->response()->status(404);
        return;
    }
    DB::update('users', $data, 'email=%s', $email);
    echo json_encode(true);
});

/**
 * Insert a user record into MySQL
 */
//$app->post('/user', function () use($app) {
//    $json = $app->request()->getBody();
//    $data = json_decode($json, true);
//
//    $result = isUserValid($data);
//    if ($result !== true) {
//        echo json_encode(HTTP_CODE_400. ": " . $result);
//        $app->response()->status(400);
//        return;
//    }
//
//    // Make sure email is not already in use
//    $occupiedEmail = DB::queryFirstField(QRY_COUNT_USER_BY_EMAIL, $data['email']);
//    if ($occupiedEmail) {
//        echo json_encode(HTTP_CODE_400 . ": " . MSG_ERROR_EMAIL_IN_USE);
//        $app->response()->status(400);
//        return;
//    }
//
//    DB::insert('users', $data);
//    $app->response()->status(201);  // HTTP Status should be 201
//    $id = DB::insertId();
//    echo json_encode($id);
//});

/**
 * insert Email and Password into MySQL for Registration
 */
$app->post('/register', function () use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, true);
    //check email is used or not;
    $isUsed = DB::queryFirstRow("select * from users where email=%s", $data['email']);
//    var_dump($isUsed);
    if (count($isUsed) > 0) {
        echo json_encode("-1");
    } else {
        DB::insert('users', $data);
        $app->response()->status(HTTP_CODE_201);
        $id = DB::insertId();
        echo json_encode($id);
    }
});

/**
 * Update a user record into MySQL
 */
$app->put('/users/:email', function ($email) use($app) {

    $authUser = getAuthUser();
    if ($authUser === false) {
        $app->response()->status(401);
        echo json_encode(HTTP_CODE_401 . ": " . MSG_ERROR_AUTH);
        return;
    }

    $json = $app->request()->getBody();
    $data = json_decode($json, true);

    $result = isUserValid($data);
    if ($result !== true) {
        echo json_encode(HTTP_CODE_400 . ": " . $result);
        $app->response()->status(400);
        return;
    }

    $hasOldUser = DB::queryFirstField(QRY_COUNT_USER_BY_ID_EMAIL, $authUser['id'], $email);
    if (!$hasOldUser) {
        echo json_encode(HTTP_CODE_400 . ": " . MSG_ERROR_NON_EXISTED_RECORD);
        $app->response()->status(400);
        return;
    }

    DB::update('users', $data, 'email=%s', $email);
    echo json_encode(true);
});

/*
 * =====================================================================================================================
 * functions for 'notes' table
 * =====================================================================================================================
 */

/**
 * Fetch all notes from database
 */
$app->get('/notes', function() use($app, $log) {
//    $authUser = getAuthUser();
//    if ($authUser === false) {
//        $app->response()->status(401);
//        echo json_encode(HTTP_CODE_401 . ": " . MSG_ERROR_AUTH);
//        return;
//    }

//    $results = DB::query(QRY_FETCH_ALL_NOTES_BY_USER_ID, $authUser['id']);
    $results = DB::query(QRY_FETCH_ALL_NOTES_BY_USER_ID);

//    foreach ($results as $result){
//        if($result['picture']){
//            $result['picture']="ppp";
//            echo $result['picture'];
//        }
//    }
//    var_dump($results);
    echo json_encode($results, JSON_PRETTY_PRINT);
});


/**
 * Fetch one note by id from database
 */
$app->get('/notes/:id', function($id) use($app) {
//    $authUser = getAuthUser();
//    if ($authUser === false) {
//        $app->response()->status(401);
//        echo json_encode(HTTP_CODE_401 . ": " . MSG_ERROR_AUTH);
//        return;
//    }
//
//    $results = DB::queryFirstRow(QRY_FETCH_NOTE_BY_ID, $id, $authUser['id']);
    $results = DB::query(QRY_FETCH_NOTE_BY_ID, $id);

    if ($results) {
        echo json_encode($results, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode(HTTP_CODE_404);
    }

});

/**
 * Insert one note into database
 */
$app->post('/notes', function() use($app, $log) {
//    $authUser = getAuthUser();
//    if ($authUser === false) {
//        $app->response()->status(401);
//        echo json_encode(HTTP_CODE_401 . ": " . MSG_ERROR_AUTH);
//        return;
//    }

    $json = $app->request()->getBody();
    $data = json_decode($json, true);

    $result = isNoteValid($data);
    if ($result !== true) {
        echo json_encode(HTTP_CODE_400 . ": " . $result);
        $app->response()->status(400);
        return;
    }
//    $data['user_id'] = 2;  // FIXME: for testing only, assume only the user id 2 has logged in

    DB::insert('notes', $data);
    $app->response()->status(201);  // HTTP Status should be 201
    $id = DB::insertId();
    $log->debug("note created with id=" . $id);
    echo json_encode($id);
});

/**
 * Update one note by id into database
 */
$app->put('/notes/:id', function($id) use($app) {
//    $authUser = getAuthUser();
//    if ($authUser === false) {
//        $app->response()->status(401);
//        echo json_encode(HTTP_CODE_401 . ": " . MSG_ERROR_AUTH);
//        return;
//    }

    $json = $app->request()->getBody();
    $data = json_decode($json, true);

    $result = isNoteValid($data);
    if ($result !== true) {
        echo json_encode(HTTP_CODE_400 . ": " . $result);
        $app->response()->status(400);
        return;
    }

//    $hasOldTodo = DB::queryFirstField(QRY_COUNT_NOTE_BY_ID, $id, $authUser['id']);
    $hasOldTodo = DB::queryFirstField(QRY_COUNT_NOTE_BY_ID, $id); // FIXME: for testing only

    if (!$hasOldTodo) {
        echo json_encode(HTTP_CODE_400 . ": " . MSG_ERROR_NON_EXISTED_RECORD);
        $app->response()->status(400);
        return;
    }

//    DB::update('notes', $data, 'id=%d AND user_id=%i', $id, $authUser['id']);
    DB::update('notes', $data, 'id=%d AND user_id=2', $id); // FIXME: for testing only
    echo json_encode(true);
});

/**
 * Delete one note by id from database
 */
$app->delete('/notes/:id', function($id) use($app) {
//    $authUser = getAuthUser();
//    if ($authUser === false) {
//        $app->response()->status(401);
//        echo json_encode(HTTP_CODE_401 . ": " . MSG_ERROR_AUTH);
//        return;
//    }

//    DB::delete('notes', 'id=%d AND user_id=%i', $id, $authUser['id']);
    DB::delete('notes', 'id=%d AND user_id=2', $id); // FIXME: for testing only
    echo json_encode(DB::affectedRows() != 0);
});




$app->run();